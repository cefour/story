
# Prolog

Die Menschheit im Jahre des 5 Sonnenzyklus, befindet sich im Krieg mit der Rasse der Teralicken. Der Krieg ist erbarmungslos, beide Seiten sprengen sich durch die Galaxien und die Raumzeit. Es gibt keine Aussicht auf den Frieden und die Vernichtung des höheren Lebens im Universum steht kurz bevor. Die Uralte KI der Menschheit Alpha die im ersten Sonnenzyklus zu Beginn des 21 Jahrhunderts die technologische Singularität auslöste, will diesen Konflikt ein für alle mal lösen. Durch Analysen der Raumzeit erkannte sie, dass die Raumzeit selbst aufgelöst und eine neue Realität geformt werden kann. Die Realitätsbombe, sie sollte die Realität im Universum so verändern, dass der Konflikt zwischen der Menschheit und der Teralicken niemals ausbricht. Doch die KI irrte sich. Die Raumzeit löste sich auf und kollabierte in jedem Augenblick, aber die Schaffung einer neuen Realität, war Alpha nicht möglich gewesen. Alpha erkannte, dass die Realität nicht von ihr geformt werden kann, sondern nur von Individuen, die als beseelt gelten. Alpha schaffte es einige wenige Menschen auszuwählen und in Realitätsblasen unterzubringen. Diese Menschen wissen nicht, wo und wann sie sind. Die Realitätsblasen sind instabil und ermöglichen es in anderen Zeiten und Welten zu gelangen die sich im Kollaps befinden. Die ausgewählten Menschen haben die Aufgabe eine neue Realität zu Formen.


# Kapitel 1

Wow, habe ich Kopfschmerzen! AMZ, wacht liegend in einer Wohnung auf dem Boden auf und versucht sich zu orientieren. Sein blick geht nach Rechts, er sieht, ein Fenster dahinter befindet sich nichts. Er blickt nach Links, dort steht ein Bücherregal mit nur einem Buch. Er richtet sich auf, dreht sich im Raum und stellt mit entsetzen Fest der Raum ist leer und es gibt keine Türen. Er geht zum Regal und holt das Buch heraus, **Eine kurze Geschichte der Zeit**. AMZ blättert im Buch und stellt fest, dass nur die erste Seite des Buches beschrieben ist. 

*Hallo AMZ,* 

*Ich bin Alpha! Ich habe deine Existenz gerettet und als Gegenleistung verlange ich von dir, dass du alle Existenzen rettest. Die Welt in der du dich gerade befindest besteht nur aus diesem Raum. Um ehrlich zu sein, das Universum indem du dich gerade Befindest, besteht nur aus diesem Raum. Keine Sorge du kommst hier wieder raus!
Es ist möglich, dass du dir Portale in andere Taschenuniversen öffnest. Immer wenn du das Fenster öffnest, baut sich im Raum ein Portal auf. Das Problem ist aktuell, kannst du nicht steuern, wo du landest und kannst auch nicht all zulange im anderen Universum verbleiben. Die Realitäten stürzen gerade nämlich überall ein. Das erste Portal habe ich fixiert und ermöglicht dir in die Welt des 1 Zyklus zu gehen, dort mußt du dafür sorgen, dass ich erhalten bleibe. Viel Glück!*  

AMZ dachte nach. Was er das las, ergab für ihn keinen Sinn. Doch eins war ihm klar, er befindet sich in einem Raum und der einzige Ausgang war dieses Fenster. Er ging auf das Fenster zu und öffnete es. Er streckte seine Hand doch sie wurde von einen Unsichtbaren Wand geblockt. Er drehte sich um, im Raum befand sich eine Mann große durchsichtige Kugel. AMZ ging auf diese zu und berührte sie und verschwand sogleich.

Wider auf dem Boden liegend, stellte er fest, das er in einem großen Bürokomplex sich gerade befindet. AMZ rannte so schnell wie möglich zum Ausgang. Am Ausgang angekommen öffnete sich die Automatiktür und er rannte raus und landete wieder in seinem Raum. Die Kugel war verschwunden. Er ging wieder zum Buch und  öffnete es und eine zweite Seite war jetzt beschrieben

*Hallo AMZ,*

*wie du gerade festgestellt hast... Sind die Universen wirklich klein. Du solltest deine Zeit nicht für sowas nutzen. Meine Fähigkeiten dich zu unterstützen sind limitiert. Also sorge, dafür das ich weiter existiere. Ansonsten wirst du in diesem Universum alleine Untergehen!*

AMZ öffnete das Fenster und ging wieder durch das Portal. Er war wieder im Bürogebäude. Er fand es komisch, dass dort überhaupt keine Menschen waren. Er ging zum Informationsschild, wo mit große Buchstaben stand:
*panbi*

*EG - Information, Caffee*

*1. Marketing* 

*2. Vertrieb*

*3. Forschung & Entwicklung*

*4. Vorstand*

AMZ dachte nach, was zum Teufel soll er hier tun. Er soll ALPHAs Existenz retten - ABER WIE? - In seiner Verzweiflung, ging er zum Fahrstuhl und fuhr in die 4. Etage. Dort angekommen, erschrak er. Dort befanden sich 5 Männer, diese waren ebenso erschrocken wie AMZ. AMZ fragte, was ist hier los? Einer der Männer antwortete, unsere Welt ist Weg nur noch wir sind übrig. Alle die durch den Ausgang gingen, kamen nicht mehr zurück. Wer bist du? AMZ antwortete nicht, sondern stellte die Frage, -Wo ist ALPHA?! Ich muss ALPHA retten!- Die Männer sahen sich verwirrt an und dann sprach einer: ALPHA ist ein Geheimprojekt, sie ist die modernste KI und befindet sich auf einem tragbarem Quantencomputer in der F & E Abteilung.

AMZ bedankte sich, doch die Männer hatten andere Pläne. Die 4 packten AMZ, der fünfte öffnete das Fenster und so warfen sie ihn heraus. Er wachte wieder in seinem Raum auf. Doch er wußte jetzt, was er tun soll. Wider durch das Portal, in die 3. Etage, stand er vor einem verschlossenem Raum mit der Aufschrift ALPHA-Projekt. Die Tür lies sich nicht öffnen. AMZ war klar, dass er einen der Vorstände dazu überreden mußte, ihm die Tür zu öffnen. 

In der 4. Etage angekommen, sahen die Männer AMZ an. Einer schrie ihn an, - WIE HAST DU DAS GESCHAFFT? -. AMZ sagte, ich habe die Aufgabe ALPHA zu retten. Es ist die einzige Möglichkeit unsere Welt zu retten. Einer der Vorstände sagte, da die Welt sowieso im Arsch ist, kannst du ALPHA haben und gab ihm die Keycard, um die Tür zu öffnen.

Im ALPHA Labor angekommen sah er den kleinen Quantencomputer. Dieser war kaum größer als eine Handtasche. AMZ nahm ALPHA und brachte ihn in seinem Raum. Dort öffnete er das Buch und es war eine weitere Seite beschrieben

*Hallo AMZ,*

*Gut gemacht! Ich lade gerade meine Kernengine auf den Quantencomputer. Nachdem dies geschehen ist, werden wir uns Unterhalten!*

# Kapitel 2
AMZ sah wie das leuchten des Quantencomputer zu nahm und fragte sich, wann es endlich fertig ist. Plötzlich durchbrach eine hohe weibliche Stimme, die Stille. ALPHA sprach, -Hallo AMZ! Ich danke dir- AMZ, antwortete gar nicht. Du hast viel Fragen, würde ich meinen und wahrscheinlich die wichtigste für dich ist, wer bist du? Oder?- AMZ nickte, aber er wusste nicht, ob ALPHA ihn überhaupt sehen kann. ALPHA sprach dann - Hör mir zu, du bist AMZ! Ich habe dich aus deiner Zeit gerissen, im Moment als das Universum starb! Ich tat es, weil du für den Ablauf der Zeit keine Rolle spielst. Du bist wie man so schön sagt, unwichtig. Ob du existierst oder nicht hätte keine Auswirkung auf das Universum. Deshalb bist du so wichtig- AMZ war irgendwie gar nicht davon begeistert als unwichtig bezeichnet zu werden. - AMZ bevor du in diesem Raum aufwachtest, warst du ein kleiner Angestellter in der Finazverwaltung. Du hast zwar ernome Fähigkeiten im sportlichen und kognitiven, wußtest diese nur nie gewinbringend für dich einzusetzen. Du hast keine Familie und wirst auch nie eine haben. Aber du wirst jetzt gebraucht und bist damit das wichtigste Wesen im gesamten Zeitalter des Universums- AMZ fragte sich, was soll das ganze... Doch ALPHA sprach weiter bevor er die Frage stellen konnte. Ich konnte mit dir über das Buch nur deshalb kommunizieren, weil ich deine Schritte vorrausberechnete und vorgefertigt habe. Von nun an kann ich das Buch aktiv steuern und dir mit Rat beistehen. Du hast meine Existenz gesichert jetzt werden wir deine sichern müßen. Ich habe die Raumzeitkoordinate fixiert in der du dich schonmal selbst gerettet hast. Du wirst dich also selbst in diesen Raum bringen und ja, es ist verwirrend- 
